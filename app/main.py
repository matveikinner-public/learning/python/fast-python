from typing import Union

from fastapi import FastAPI

from app.prisma import prisma
from app.users import router as usersRouter

app = FastAPI()

app.include_router(usersRouter, prefix="/api")

@app.on_event("startup")
async def startup():
  await prisma.connect()

@app.on_event("shutdown")
async def shutdown():
  await prisma.disconnect()

@app.get("/")
def read_root():
  return {"Hello": "World"}