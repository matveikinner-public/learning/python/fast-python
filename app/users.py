from fastapi import APIRouter
from app.prisma import prisma
from pydantic import BaseModel

router = APIRouter()

class CreateUserDto(BaseModel):
  firstName: str
  lastName: str
  email: str

@router.get("/users/", tags=["users"])
async def read_users():
  users = await prisma.user.find_many()

  return users

@router.get("/users/{userId}", tags=["users"])
async def read_user(userId: str):
  user = await prisma.user.find_unique(where={"id": userId})

  return user

@router.post("/users/", tags=["users"])
async def create_user(user: CreateUserDto):
  user = await prisma.user.create({
    "firstName": user.firstName,
    "lastName": user.lastName,
    "email": user.email,
  })

  return user
