#!/bin/sh

# Prerequisites requires to install pipreqs (pip install pipreqs)
# See https://github.com/bndr/pipreqs
pipreqs $PWD --force