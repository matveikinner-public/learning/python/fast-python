FROM python:3.10-alpine

# See official /opt directory documentation at Linux Filesystem Hierarchy
# https://tldp.org/LDP/Linux-Filesystem-Hierarchy/html/opt.html
WORKDIR /opt/fast-python

# Copy Python application requirements to install / run this app
COPY ./requirements.txt .

RUN pip install --no-cache-dir --upgrade -r ./requirements.txt

COPY ./app ./app

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]